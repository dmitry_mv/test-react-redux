import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../Styles/CitiesList.css';

class ListCities extends Component {

    onDeleteCity = (cityId)=> {
        this.props.onDeleteCity(cityId);
    };

    render() {
        return (
            <div className="cities-wrapper">
                <ul className="cities-wrapper__ul">
                    {this.props.citiesList.map((city, index) =>
                        <li className="cities-wrapper__ul__li" key={index}>
                            <div onClick={()=>{this.onDeleteCity(city.id)}} className="cities-wrapper__ul__li__remove">x</div>
                            <div className="cities-wrapper__ul__li__name">{city.name}</div>
                            <div className="cities-wrapper__ul__li__img">
                                <img className="cities-wrapper__ul__li__img__element" src={city.icon_url} alt="sunny"/>
                            </div>
                            <div
                                className="cities-wrapper__ul__li__temperature">{city.temperature > 0 ? '+' + city.temperature : city.temperature}&nbsp;°C
                            </div>
                            <div className="cities-wrapper__ul__li__wind">Ветер: {city.wind} м/с</div>
                            <div className="cities-wrapper__ul__li__pressure">Давление: {city.pressure} мм</div>
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}

export default connect(
    state => ({
        citiesList: state.citiesList.filter(city => city.temperature >= state.filterCities)
    }),
    dispatch => ({
        onDeleteCity: (cityId) => {
            dispatch({type: 'DELETE_CITY', payload: {id: cityId}});
        }

    }))(ListCities);