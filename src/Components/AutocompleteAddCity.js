import React, { Component } from 'react';
//import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import '../Styles/AutocompleteCity.css';

class AutocompleteAddCity extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputCity: null,
            inputValue: '',
            selectedCity: {},
            showDropdown: false,
            cities: [],
            isLoading: false
        };
    }

    json = (url, callback) => {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                callback(xhr.responseText);
            }
        };
        xhr.send();
    };
    jsonp = (url, callback) => {
        window['onFetchComplete'] = callback;
        let script = document.createElement("script");
        script.type = "text/javascript";
        script.src = url + "&callback=onFetchComplete";
        document.body.appendChild(script);
        script.onload = function () {
            this.remove();
        };
    };
    onFetchComplete = (data) => {
        this.setState({
            ...this.state,
            isLoading: false,
            cities: data.RESULTS
        });
    };

    getRandomPressure = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    callbackGetCityModel = (response) => {
        let data = JSON.parse(response);
        let forecast = data.forecast.simpleforecast.forecastday[0];
        return {
            id: this.state.selectedCity.zmw,
            name: this.state.selectedCity.name,
            temperature: +forecast.high.celsius,
            icon_url: forecast.icon_url,
            wind: (forecast.maxwind.kph * 0.277778).toFixed(2),
            pressure: this.getRandomPressure(750, 820).toFixed(0)
        };
    };
    onSubmit = (e) => {
        e.preventDefault();
        this.setState({
            ...this.state,
            showDropdown: false
        });
        if (this.state.selectedCity.name === this.state.inputValue) {
            //1. Получаем данные прогноза по запросу
            this.json("http://api.wunderground.com/api/0dd4e32a315315e6/forecast/lang:RU/q/zmw:" + this.state.selectedCity.zmw + ".json", (response) => {
                this.props.onAddCity(this.callbackGetCityModel(response));
                this.inputCity.value = '';
            });
            //2. Меняем шаблон вывода
            /*
             this.props.onAddCity(this.state.selectedCity);
             this.inputCity.value = '';
             */
        }
    };
    onFocus = (e) => {
        this.setState({
            ...this.state,
            showDropdown: true
        });
    };
    onBlur = (e) => {
        setTimeout(()=> {
            this.setState({
                ...this.state,
                showDropdown: false
            });
        }, 1000);
    };
    onChange = (e) => {
        this.setState({
            ...this.state,
            inputValue: e.target.value,
            isLoading: true
        });
        this.jsonp("https://autocomplete.wunderground.com/aq?query=" + e.target.value + "&c=RU&type=city&format=json&cb=onFetchComplete", this.onFetchComplete.bind(this));
    };

    selectCity = (e, city) => {
        this.inputCity.value = city.name;
        this.setState({
            ...this.state,
            inputValue: city.name,
            selectedCity: city,
            showDropdown: false
        });
    };

    render() {

        return (
            <div className="autocomplete-wrapper">
                <form onSubmit={this.onSubmit} autoComplete="off" noValidate>
                    <div className="autocomplete-wrapper__input">
                        <input
                            className="autocomplete-wrapper__input__element"
                            type="text"
                            placeholder="Введите город"
                            ref={(input)=>{this.inputCity = input}}
                            onChange={this.onChange}
                            onFocus={this.onFocus}
                            onBlur={this.onBlur}
                        />

                        {this.state.showDropdown ?
                            <div className="autocomplete-wrapper__input__dropdown">
                                {this.state.isLoading ?
                                    <img src="./img/loader.gif"
                                         className="autocomplete-wrapper__input__dropdown__loader" alt="Loader"/>
                                    :
                                    <ul className="autocomplete-wrapper__input__dropdown__ul">
                                        {this.state.cities
                                            .filter(city => city.name.includes(this.state.inputValue))
                                            .map((city, index) =>
                                                <li
                                                    onClick={(e) => this.selectCity(e, city)}
                                                    key={index}
                                                    className="autocomplete-wrapper__input__dropdown__ul__li">
                                                    { city.name }
                                                </li>
                                            )}
                                    </ul>
                                }
                            </div>
                            : null}

                    </div>
                    <div className="autocomplete-wrapper__submit">
                        <button type="submit" className="autocomplete-wrapper__submit__element"> +</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        onAddCity: (city) => {
            dispatch({type: 'ADD_CITY', payload: city});
        }

    }))(AutocompleteAddCity);