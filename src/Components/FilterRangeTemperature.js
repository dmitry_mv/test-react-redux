import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import '../Styles/FilterRangeTemperature.css';

class FilterRangeTemperature extends Component {

    static propTypes = {
        range: PropTypes.shape({
            min: PropTypes.number.isRequired,
            max: PropTypes.number.isRequired,
            step: PropTypes.number.isRequired,
            value: PropTypes.number.isRequired
        })
    };

    constructor(props) {
        super(props);
        this.state = {
            rangeValue: props.range.value
        };
    }

    onChangeRange = (e) => {
        this.setState({rangeValue: e.target.value});
        this.props.changeFilterRange(e.target.value);
    };

    render() {
        return (
            <div className="range-wrapper">
                <div className="range-wrapper__title">
                    <label>Где сейчас теплее, чем</label>
                </div>
                <div className="range-wrapper__slider">
                    <input className="range-wrapper__slider__input"
                           type="range"
                           onChange={this.onChangeRange}
                           min={this.props.range.min}
                           max={this.props.range.max}
                           step={this.props.range.step}
                           value={this.state.rangeValue}
                    />
                </div>
                <div className="range-wrapper__value">
                    {this.state.rangeValue > 0 ? '+' + this.state.rangeValue : this.state.rangeValue} °C

                </div>
            </div>
        );
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        changeFilterRange: (name) => {
            dispatch({type: 'CHANGE_FILTER', payload: name});
        }
    }))(FilterRangeTemperature);