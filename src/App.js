import React, { Component } from 'react';
import { connect } from 'react-redux';

import AutocompleteAddCity from './Components/AutocompleteAddCity';
import FilterRangeTemperature from './Components/FilterRangeTemperature';
import ListCities from './Components/ListCities';

import './Styles/App.css';

/*
const citiesList = [
    {
        id: 1,
        name: 'Москва',
        temperature: 7,
        wind: 5,
        pressure: 721
    },
    {
        id: 2,
        name: 'Санкт-Петербург',
        temperature: 20,
        wind: 2,
        pressure: 740
    },
    {
        id: 3,
        name: 'Ярославль',
        temperature: -20,
        wind: 6,
        pressure: 752
    },
    {
        id: 4,
        name: 'Тагил',
        temperature: -40,
        wind: 2,
        pressure: 800
    },
    {
        id: 5,
        name: 'Сочи',
        temperature: 10,
        wind: 3,
        pressure: 754
    }
];
*/
const rangeTemperature = {
    max: 50,
    min: -50,
    step: 1,
    value: -50
};

class App extends Component {
    render() {

        return (
            <div className="app-wrapper">
                <AutocompleteAddCity />
                <FilterRangeTemperature range={rangeTemperature} />
                <ListCities />
            </div>
        );
    }
}

export default connect(
    state => ({
    }),
    dispatch => ({

    }))(App);
