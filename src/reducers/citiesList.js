const initialState = [];

export default function citiesList(state = initialState, action) {
    if (action.type === "ADD_CITY"){
        for(let i = 0; i < state.length; i++){
            if (state[i]['id'] === action.payload.id){
                return state;
            }
        }
        return [
            ...state,
            action.payload
        ];
    }else if (action.type === "DELETE_CITY") {
        for (let i = 0; i < state.length; i++){
            if (state[i]['id'] === action.payload.id){
                state.splice(i, 1);
                break;
            }
        }
        return [...state];
    }
    return state;
}