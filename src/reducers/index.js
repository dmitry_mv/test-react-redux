import { combineReducers } from 'redux';
import filterCities from './filterCities';
import citiesList from './citiesList';

export default combineReducers({
    filterCities,
    citiesList
});