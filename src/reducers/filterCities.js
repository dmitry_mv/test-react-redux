const initialState = -50;

export default function filterCities(state = initialState, action) {
    if (action.type === "CHANGE_FILTER"){
        return action.payload;
    }
    return state;
}